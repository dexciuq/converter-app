package com.dexciuq.converterlib.data.model

data class FluctuationDto(
    val rates: Map<String, CurrencyDto>,
)