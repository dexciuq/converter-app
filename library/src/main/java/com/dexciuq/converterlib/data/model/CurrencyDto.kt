package com.dexciuq.converterlib.data.model

import com.google.gson.annotations.SerializedName

data class CurrencyDto (
    val change: Double,
    @SerializedName("end_rate") val endRate: Double,
)