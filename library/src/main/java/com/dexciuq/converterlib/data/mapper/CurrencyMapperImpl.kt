package com.dexciuq.converterlib.data.mapper

import com.dexciuq.converterlib.data.model.CurrencyDto
import com.dexciuq.converterlib.domain.model.Currency

class CurrencyMapperImpl : CurrencyMapper {
    override fun toDomain(entry: Map.Entry<String, CurrencyDto>) = Currency(
        name = entry.key,
        currentRate = entry.value.endRate,
        changes = entry.value.change,
    )
}