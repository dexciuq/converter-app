package com.dexciuq.converterlib.data.model

data class ConversionDto(
    val result: Double,
)
