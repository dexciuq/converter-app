package com.dexciuq.converterapp.view

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.dexciuq.converterapp.R
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class AuthActivityTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(AuthActivity::class.java)

    private lateinit var scenario: ActivityScenario<AuthActivity>

    @Before
    fun setUp() {
        scenario = activityRule.scenario
    }

    @Test
    fun performLogin_BlankError() {
        onView(withId(R.id.edit_text_username))
            .perform(typeText(""))
        onView(withId(R.id.edit_text_password))
            .perform(typeText("     "), closeSoftKeyboard())
        onView(withId(R.id.login_button))
            .perform(click())

        val expectedErrorMessage = "Fill all fields, please"
        onView(withId(R.id.error))
            .check(matches(isDisplayed()))
            .check(matches(withText(expectedErrorMessage)))
    }

    @Test
    fun performLogin_CredentialError() {
        onView(withId(R.id.edit_text_username))
            .perform(typeText("invalid_username"))
        onView(withId(R.id.edit_text_password))
            .perform(typeText("invalid_password"), closeSoftKeyboard())
        onView(withId(R.id.login_button))
            .perform(click())

        val expectedErrorMessage = "Wrong username or password"
        onView(withId(R.id.error))
            .check(matches(isDisplayed()))
            .check(matches(withText(expectedErrorMessage)))
    }

    @Test
    fun performLogin_Success() {
        onView(withId(R.id.edit_text_username))
            .perform(typeText("admin"))
        onView(withId(R.id.edit_text_password))
            .perform(typeText("password"), closeSoftKeyboard())
        onView(withId(R.id.login_button))
            .perform(click())

        onView(withId(R.id.main_activity_layout))
            .check(matches(isDisplayed()))
    }
}