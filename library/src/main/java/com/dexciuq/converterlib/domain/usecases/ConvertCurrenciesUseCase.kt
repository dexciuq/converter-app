package com.dexciuq.converterlib.domain.usecases

import com.dexciuq.converterlib.domain.repository.CurrencyRepository

class ConvertCurrenciesUseCase(
    private val currencyRepository: CurrencyRepository
) {
    suspend operator fun invoke(from: String, to: String, amount: Double): Double =
        currencyRepository.convertCurrencies(from, to, amount)
}