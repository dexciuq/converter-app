package com.dexciuq.converterlib.data.network

import com.dexciuq.converterlib.data.model.ConversionDto
import com.dexciuq.converterlib.data.model.FluctuationDto
import com.dexciuq.converterlib.data.model.SymbolsDto
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApiService {

    @GET("exchangerates_data/symbols")
    suspend fun currencySymbols(): SymbolsDto

    @GET("exchangerates_data/convert")
    suspend fun currencyConvert(
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("amount") amount: Double
    ): ConversionDto

    @GET("/exchangerates_data/fluctuation")
    suspend fun currencyFluctuation(
        @Query("start_date") startDate: String,
        @Query("end_date") endDate: String,
        @Query("base") base: String,
    ): FluctuationDto
}