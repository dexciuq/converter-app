package com.dexciuq.converterapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dexciuq.converterlib.data.mapper.CurrencyMapperImpl
import com.dexciuq.converterlib.data.datasources.ExchangeRateApiDataSource
import com.dexciuq.converterlib.domain.repository.CurrencyRepositoryImpl
import com.dexciuq.converterlib.domain.usecases.GetCurrenciesUseCase
import com.dexciuq.converterlib.presentation.mapper.UICurrencyMapper
import com.dexciuq.converterlib.presentation.mapper.UICurrencyMapperImpl
import com.dexciuq.converterlib.presentation.model.UICurrency
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(
    private val getCurrenciesUseCase: GetCurrenciesUseCase = defaultGetCurrenciesUseCase,
    private val mapper: UICurrencyMapper = defaultMapper,
) : ViewModel() {

    private val _currencies = MutableLiveData<List<UICurrency>>()
    val currencies: LiveData<List<UICurrency>> = _currencies

    fun getCurrencies() = viewModelScope.launch(Dispatchers.IO) {
        _currencies.value = getCurrenciesUseCase().map(mapper::toUICurrency)
    }

    companion object {
        private val defaultMapper = UICurrencyMapperImpl()
        private val defaultRepository = CurrencyRepositoryImpl(ExchangeRateApiDataSource(CurrencyMapperImpl()))
        private val defaultGetCurrenciesUseCase = GetCurrenciesUseCase(defaultRepository)
    }
}