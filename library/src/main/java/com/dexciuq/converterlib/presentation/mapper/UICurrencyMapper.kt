package com.dexciuq.converterlib.presentation.mapper

import com.dexciuq.converterlib.domain.model.Currency
import com.dexciuq.converterlib.presentation.model.UICurrency

interface UICurrencyMapper {
    fun toUICurrency(currency: Currency): UICurrency
}