package com.dexciuq.converterapp.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dexciuq.converterapp.model.AuthData
import com.nhaarman.mockitokotlin2.any
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class AuthViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var authViewModel: AuthViewModel

    @Mock
    private lateinit var dataObserver: Observer<AuthData>

    @Mock
    private lateinit var errorObserver: Observer<String>

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        authViewModel = AuthViewModel()
        authViewModel.data.observeForever(dataObserver)
        authViewModel.error.observeForever(errorObserver)
    }

    @After
    fun tearDown() {
        authViewModel.data.removeObserver(dataObserver)
        authViewModel.error.removeObserver(errorObserver)
    }

    @Test
    fun `performLogin with valid credentials calls data update`() {
        val validUsername = "admin"
        val validPassword = "password"
        authViewModel.performLogin(validUsername, validPassword)
        verify(dataObserver).onChanged(any())
    }

    @Test
    fun `performLogin with invalid credentials calls error update`() {
        val invalidUsername = "user"
        val invalidPassword = "pass"

        authViewModel.performLogin(invalidUsername, invalidPassword)
        verify(errorObserver).onChanged("Wrong username or password")
    }

    @Test
    fun `performLogin with blank credentials calls error update`() {
        val blankUsername = ""
        val blankPassword = "   "
        authViewModel.performLogin(blankUsername, blankPassword)
        verify(errorObserver).onChanged("Fill all fields, please")
    }

    @Test
    fun `performLogin with valid credentials updates data correctly`() {
        val validUsername = "admin"
        val validPassword = "password"
        authViewModel.performLogin(validUsername, validPassword)
        val authData = authViewModel.data.value
        assertTrue(System.currentTimeMillis() < authData?.expireAt!!)
    }

    @Test
    fun `performLogin with invalid credentials does not update data`() {
        val invalidUsername = "user"
        val invalidPassword = "pass"
        authViewModel.performLogin(invalidUsername, invalidPassword)
        val authData = authViewModel.data.value
        assertNull(authData)
    }

    @Test
    fun `generateAccessToken should return a non-empty string`() {
        val accessToken = authViewModel.generateAccessToken()
        assertTrue(accessToken.isNotEmpty())
    }

    @Test
    fun `generateExpireAt should return a future timestamp`() {
        val expireAt = authViewModel.generateExpireAt()
        assertTrue(expireAt > System.currentTimeMillis())
    }

    @Test
    fun `validateCredentials with correct username and password should return true`() {
        val username = "admin"
        val password = "password"
        val isValid = authViewModel.validateCredentials(username, password)
        assertTrue(isValid)
    }

    @Test
    fun `validateCredentials with incorrect username and password should return false`() {
        val username = "admin"
        val password = "wrongpassword"
        val isValid = authViewModel.validateCredentials(username, password)
        assertFalse(isValid)
    }
}