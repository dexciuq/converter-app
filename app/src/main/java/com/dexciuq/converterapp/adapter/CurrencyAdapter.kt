package com.dexciuq.converterapp.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.converterapp.R
import com.dexciuq.converterapp.databinding.LayoutItemBinding
import com.dexciuq.converterlib.presentation.model.UICurrency

class CurrencyAdapter : ListAdapter<UICurrency, CurrencyAdapter.ViewHolder>(CurrencyDiffCallback()){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    inner class ViewHolder(
        private val binding: LayoutItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(currency: UICurrency) {
            binding.name.text = currency.name
            binding.rate.text = "Rate: ${currency.currentRate}"
            binding.changes.text = if (currency.isPositive) "+${currency.changes}"
                                   else currency.changes
            binding.icon.setImageResource(
                if (currency.isPositive) R.drawable.ic_up
                else R.drawable.ic_down
            )
            binding.root.setBackgroundResource(
                if (currency.isPositive) R.drawable.green_gradient_background
                else R.drawable.red_gradient_background
            )
        }
    }
}