package com.dexciuq.converterlib.presentation.mapper

import com.dexciuq.converterlib.domain.model.Currency
import com.dexciuq.converterlib.presentation.model.UICurrency

class UICurrencyMapperImpl : UICurrencyMapper {
    override fun toUICurrency(currency: Currency) = UICurrency(
        name = currency.name,
        currentRate = currency.currentRate.toString(),
        changes = currency.changes.toString(),
        isPositive = currency.changes > 0,
    )
}