package com.dexciuq.converterlib.data.datasources

import com.dexciuq.converterlib.domain.model.Currency

interface CurrencyDataSource {
    suspend fun getSymbols(): List<String>
    suspend fun getCurrencies(): List<Currency>
    suspend fun convertCurrencies(from: String, to: String, amount: Double): Double
}