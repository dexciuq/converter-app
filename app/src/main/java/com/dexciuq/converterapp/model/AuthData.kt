package com.dexciuq.converterapp.model

data class AuthData(
    val token: String,
    val expireAt: Long,
)
