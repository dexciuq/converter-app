package com.dexciuq.converterlib.domain.repository

import com.dexciuq.converterlib.domain.model.Currency

interface CurrencyRepository {
    suspend fun getSymbols(): List<String>
    suspend fun getCurrencies(): List<Currency>
    suspend fun convertCurrencies(from: String, to: String, amount: Double): Double
}
