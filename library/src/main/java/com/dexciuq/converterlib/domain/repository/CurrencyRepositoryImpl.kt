package com.dexciuq.converterlib.domain.repository

import com.dexciuq.converterlib.data.datasources.CurrencyDataSource
import com.dexciuq.converterlib.domain.model.Currency

class CurrencyRepositoryImpl(
    private val dataSource: CurrencyDataSource,
) : CurrencyRepository {

    override suspend fun getSymbols(): List<String> = dataSource.getSymbols()

    override suspend fun getCurrencies(): List<Currency> = dataSource.getCurrencies()

    override suspend fun convertCurrencies(from: String, to: String, amount: Double): Double =
        dataSource.convertCurrencies(from, to, amount)

}