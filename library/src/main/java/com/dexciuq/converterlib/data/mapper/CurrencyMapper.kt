package com.dexciuq.converterlib.data.mapper

import com.dexciuq.converterlib.data.model.CurrencyDto
import com.dexciuq.converterlib.domain.model.Currency

interface CurrencyMapper {
    fun toDomain(entry: Map.Entry<String, CurrencyDto>): Currency
}