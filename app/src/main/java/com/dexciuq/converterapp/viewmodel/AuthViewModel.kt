package com.dexciuq.converterapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dexciuq.converterapp.model.AuthData
import java.util.UUID

class AuthViewModel : ViewModel() {

    private val _data = MutableLiveData<AuthData>()
    val data: LiveData<AuthData> = _data

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    fun performLogin(username: String, password: String) {
        if (username.isBlank() || password.isBlank()) {
            _error.value = "Fill all fields, please"
            return
        }
        if (!validateCredentials(username, password)) {
            _error.value = "Wrong username or password"
            return
        }
        _data.value = AuthData(
            token = generateAccessToken(),
            expireAt = generateExpireAt(),
        )
    }

    fun generateAccessToken(): String = UUID.randomUUID().toString()

    fun generateExpireAt(): Long = System.currentTimeMillis() + 10 * 60 * 1000

    fun validateCredentials(username: String, password: String) =
        username == DEFAULT_USERNAME && password == DEFAULT_PASSWORD

    companion object {
        private const val DEFAULT_USERNAME = "admin"
        private const val DEFAULT_PASSWORD = "password"
    }
}