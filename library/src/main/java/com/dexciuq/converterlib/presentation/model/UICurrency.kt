package com.dexciuq.converterlib.presentation.model

data class UICurrency(
    val name: String,
    val currentRate: String,
    val changes: String,
    val isPositive: Boolean = false,
)