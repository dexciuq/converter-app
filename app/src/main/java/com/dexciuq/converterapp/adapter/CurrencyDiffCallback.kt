package com.dexciuq.converterapp.adapter

import androidx.recyclerview.widget.DiffUtil
import com.dexciuq.converterlib.presentation.model.UICurrency

class CurrencyDiffCallback : DiffUtil.ItemCallback<UICurrency>() {

    override fun areItemsTheSame(oldItem: UICurrency, newItem: UICurrency): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: UICurrency, newItem: UICurrency): Boolean {
        return oldItem == newItem
    }
}