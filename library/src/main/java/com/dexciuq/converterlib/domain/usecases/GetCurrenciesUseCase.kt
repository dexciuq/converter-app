package com.dexciuq.converterlib.domain.usecases

import com.dexciuq.converterlib.domain.model.Currency
import com.dexciuq.converterlib.domain.repository.CurrencyRepository

class GetCurrenciesUseCase(
    private val currencyRepository: CurrencyRepository
) {
    suspend operator fun invoke(): List<Currency> = currencyRepository.getCurrencies()
}