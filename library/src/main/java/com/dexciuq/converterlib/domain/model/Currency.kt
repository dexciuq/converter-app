package com.dexciuq.converterlib.domain.model

data class Currency (
    val name: String,
    val currentRate: Double,
    val changes: Double
)