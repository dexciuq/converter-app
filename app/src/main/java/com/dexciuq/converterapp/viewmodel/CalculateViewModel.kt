package com.dexciuq.converterapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dexciuq.converterlib.data.mapper.CurrencyMapperImpl
import com.dexciuq.converterlib.data.datasources.ExchangeRateApiDataSource
import com.dexciuq.converterlib.domain.repository.CurrencyRepositoryImpl
import com.dexciuq.converterlib.domain.usecases.ConvertCurrenciesUseCase
import com.dexciuq.converterlib.domain.usecases.GetSymbolsUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CalculateViewModel(
    private val getSymbolsUseCase: GetSymbolsUseCase = defaultGetSymbolsUseCase,
    private val convertCurrenciesUseCase: ConvertCurrenciesUseCase = defaultConvertCurrenciesUseCase,
) : ViewModel() {

    private val _symbols = MutableLiveData<List<String>>()
    val symbols: LiveData<List<String>> = _symbols

    private val _result = MutableLiveData<Double>()
    val result: LiveData<Double> = _result

    fun getSymbols() = viewModelScope.launch(Dispatchers.IO) {
        _symbols.postValue(getSymbolsUseCase.invoke())
    }

    fun convertCurrencies(from: String, to: String, amount: Double) =
        viewModelScope.launch(Dispatchers.IO) {
            _result.postValue(
                convertCurrenciesUseCase.invoke(from, to, amount)
            )
        }


    companion object {
        private val defaultRepository = CurrencyRepositoryImpl(ExchangeRateApiDataSource(CurrencyMapperImpl()))
        private val defaultGetSymbolsUseCase = GetSymbolsUseCase(defaultRepository)
        private val defaultConvertCurrenciesUseCase = ConvertCurrenciesUseCase(defaultRepository)
    }
}