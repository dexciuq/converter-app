package com.dexciuq.converterapp.view

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import com.dexciuq.converterapp.databinding.ActivityCalculateBinding
import com.dexciuq.converterapp.viewmodel.CalculateViewModel

class CalculateActivity : AppCompatActivity() {

    private val binding by lazy { ActivityCalculateBinding.inflate(layoutInflater) }
    private val viewModel: CalculateViewModel by viewModels()
    private lateinit var from: String
    private lateinit var to: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setLiveDataObservers()
        setOnClickListeners()

        viewModel.getSymbols()
    }

    @SuppressLint("SetTextI18n")
    private fun setLiveDataObservers() {
        viewModel.result.observe(this) {
            binding.textView.text = "Result: $it"
        }
        viewModel.symbols.observe(this) {
            val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, it)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.fromSpinner.adapter = adapter
            binding.toSpinner.adapter = adapter
        }
    }

    private fun setOnClickListeners() {
        binding.calculate.setOnClickListener {
            viewModel.convertCurrencies(from, to , binding.amount.text.toString().toDouble())
        }

        binding.fromSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                from = parent.getItemAtPosition(pos) as String
            }
            override fun onNothingSelected(parent: AdapterView<*>) {
                from = parent.getItemAtPosition(0) as String
            }
        }

        binding.toSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                to = parent.getItemAtPosition(pos) as String
            }
            override fun onNothingSelected(parent: AdapterView<*>) {
                to = parent.getItemAtPosition(0) as String
            }
        }
    }
}