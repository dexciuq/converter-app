package com.dexciuq.converterapp.view

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.dexciuq.converterapp.databinding.ActivityAuthBinding
import com.dexciuq.converterapp.viewmodel.AuthViewModel

class AuthActivity : AppCompatActivity() {

    private val binding by lazy { ActivityAuthBinding.inflate(layoutInflater) }
    private val viewModel: AuthViewModel by viewModels()
    private lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        prefs = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        if (prefs.getLong(EXPIRE_AT, -1) > System.currentTimeMillis()) {
            intentToMainActivity()
        }

        binding.loginButton.setOnClickListener {
            val username = binding.editTextUsername.text.toString()
            val password = binding.editTextPassword.text.toString()
            val inputMethodManager = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(binding.editTextPassword.windowToken, 0)
            viewModel.performLogin(username, password)
        }

        viewModel.error.observe(this) {
            binding.error.text = it
            binding.error.isVisible = true
        }

        viewModel.data.observe(this) {
            saveToken(it.token, it.expireAt)
            intentToMainActivity()
        }
    }

    private fun saveToken(token: String, expireAt: Long) {
        with(prefs.edit()) {
            putString(ACCESS_TOKEN, token)
            putLong(EXPIRE_AT, expireAt)
            apply()
        }
    }

    private fun intentToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    companion object {
        private const val SHARED_PREFS = "converter"
        private const val ACCESS_TOKEN = "access_token"
        private const val EXPIRE_AT = "expired_at"
    }
}