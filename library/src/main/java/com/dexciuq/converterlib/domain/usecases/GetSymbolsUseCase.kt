package com.dexciuq.converterlib.domain.usecases

import com.dexciuq.converterlib.domain.repository.CurrencyRepository

class GetSymbolsUseCase(
    private val currencyRepository: CurrencyRepository
) {
    suspend operator fun invoke(): List<String> = currencyRepository.getSymbols()
}