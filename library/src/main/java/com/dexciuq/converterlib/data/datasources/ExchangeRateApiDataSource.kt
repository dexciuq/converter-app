package com.dexciuq.converterlib.data.datasources

import android.annotation.SuppressLint
import com.dexciuq.converterlib.data.mapper.CurrencyMapper
import com.dexciuq.converterlib.data.network.RetrofitClient.currencyApiService
import com.dexciuq.converterlib.domain.model.Currency
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class ExchangeRateApiDataSource(
    private val mapper: CurrencyMapper,
) : CurrencyDataSource {

    override suspend fun getSymbols(): List<String> =
        currencyApiService.currencySymbols().symbols.keys.toList()

    override suspend fun getCurrencies(): List<Currency> {
        val (start, end) = getStartAndEndDate()
        val dto = currencyApiService.currencyFluctuation(start, end, BASE)
        return dto.rates.entries.map(mapper::toDomain)
    }

    override suspend fun convertCurrencies(from: String, to: String, amount: Double): Double =
        currencyApiService.currencyConvert(from, to, amount).result

    @SuppressLint("NewApi")
    private fun getStartAndEndDate(): Pair<String, String> {
        val currentDate = LocalDate.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        val today = currentDate.format(formatter)
        val yesterday = currentDate.minusDays(1).format(formatter)
        return yesterday to today
    }

    companion object {
        private const val BASE = "EUR"
    }
}