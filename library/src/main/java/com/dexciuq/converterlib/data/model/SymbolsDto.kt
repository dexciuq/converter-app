package com.dexciuq.converterlib.data.model

data class SymbolsDto(
    val symbols: Map<String, String>,
)
